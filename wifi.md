# Wireless penetration test

## 关键字

```
WiFi Wlan 802.11 802.11a/b/g/n/ac WEP WPA WPA2 WPS BT5 Beini Kali aircrack-ng
reaver kismit pixel-duck
```

## 简介

这里简单的科普下WiFi渗透的常识。

WiFi 的基础就是802.11协议族，该协议族是物理层和链路层上的一套协议，包括了数据帧格式的规范和各种各样安全功能上的规范。
其中的核心规范就是802.11中的a，b，g，n，ac规范。这几个规范定义了WiFi的工作的频段和速度。

WiFi破解主要就是针对WiFi上面的几种安全机制进行破解，常见的安全功能有WEP，WPA，WPA2，WPS。所以只要能够突破这几种安全机制即可。

其中WEP机制是最弱的机制，只要能够截获到足够多的数据包就可以了。WPA/WPA2机制比较安全，这方面只能从弱口令方面入手了，没错，你没猜错，
拼人品的时刻到了。不过在拼人品之前，可以先尝试破解WPS，WPS功能其实更多的是一个方便的WiFi配置管理的协议，配置里面就包括了口令。
但是WPS协议脑残在他又有一个类似与口令的PIN码，共8位，分两次验证，前4位一次，后3位一次，最后以为校验码，确定了前7位就确定了最后一位。总共11000种可能。
所以如果路由起开启了WPS协议，那么可以很稳定在两到三个钟头内就拿下一个WiFi，值得普天同庆的是，WPS默认出厂开启，并且一般用户不知道那个是什么，
也就没有关。但是要注意的是WPS协议暴力破解尝试过快，路由器的WPS就挂了，非重启不可恢复。如果WPS破解不了，那么最后才会去考虑暴力破解WPA。
WPA破解首先通过抓包来获取手机，电脑这些客户端和路由器在进行连接刚开始时的时候的一个用来身份验证的握手包。这个握手包可以通过发送解除关联
的数据包来强制让别人断开连接，然后重新连接的时候抓取到，来来回回总共四个包，也叫4次握手包（shakehand）。然后就是暴力破解了，这个考人品，
大家都懂。通常这种通过密码的哈希来反向破解密码的方法有暴力破解，字典破解，字典破解改进的查表法，彩虹表法，一般我们就止步于字典破解了。
高级的查表法和彩虹表法，当然有一些组织有实现，甚至可以买到光盘，但是那个表动不动就是几十G，所以，一般普通群众中，这个东西很难传播开来。

破解WiFi需要一个Linux系统，推荐Kali（可以下那个不到一个G的精简版本，直接写到u盘上，u盘启动，没有必要安装到硬盘）
，Kali中所有工具都在仓库里面，自行安装即可。Windows理论上可以，就是用起来没有大牛水平，处理不了各种各样的bug。

破解的工具就是一个支持monitor并且支持injection的无线网卡，几个命令行工具（有图形话化的，不会用），主要是`aircrack-ng`,`reaver`,`bully`。
其他的还有`kismit`,`wifite`什么什么的，`wifite`是aircrack-ng的一个命令行图形界面（是命令行中的图形界面，没看错）。
`aircrack-ng`是抓包，破解WEP，WPA的工具，`reaver`和`bully`是专门破解WPS的工具。

`aircrack-ng`是一个套件，里面有`airmon-ng`用来将网卡调成mointor模式（抓包模式），`airodump-ng`进行具体的抓包操作，
`aireplay-ng`用来向无线网络中发送数据包（传说中的注入功能），`aircrack-ng`用来破解WEP和WPA握手包。这几个是常用的，
还有其他乱七八糟的的东西参见具体的文档。

`reaver`中有`wash`和`reaver`两个命令，`wash`命令用来扫描出带有WPS功能的热点，`reaver`命令用来进行具体的WPS破解工作。

### WiFi中的常见名词

* IBSS ad-hoc - 点对点连接，用来两个终端设备，比如两台笔记本电脑直接相连，而不需要第三方的路由器的一个组网方式，一般设备都支持，但是没看到什么人用。
* BSS - Basic Service Set 简单来讲就是一个路由器组成的网络
* BSSID - BSS的ID 意思就是路由器的MAC
* ESS - Extended Service Set 像CMCC-EDU那种由多个AP（算是路由器）组成的一个无线网络，因为有多个AP吗，所以叫Extended。
* ESSID - 一个字符串，网络名，比如CMCC-EDU
* SSID - 应该是BSSID和ESSID的统称吧
* Station - 连接到路由器的终端设备，就是笔记本电脑，手机，平板。
* Mointor - 网卡物理上支持的一种特性，就是那种能够把接收到的所有的数据包都截获下来而不是按照规范来做事的一种工作模式。就像是群里面那些潜水的。
* Injection - 就是那些潜水的跑出来忽然说了句话的意思。表示网卡在monitor模式下工作的时候能够往网络中注入一些数据包。
* Channel - 网卡的工作的信道，规范中是14个（工作在2.4GHz到2.48GHz下），中国是规定是13个，用数字1-13来表示。

### 工具的版本问题

aircrack-ng有稳定的官方，所以没什么版本之间的问题。但是reaver和bully两个工具的官方地址不好找，当然，在Kali中直接就是最新的版本。
reaver最早托管在google-code上面，这个网站先不说原来要翻墙，现在都已经被google关闭了，不知道还能不能访问到。bully原来托管在github上，
然后被举报了，仓库换了个地址，不知道还会不会被举报。reaver和bully的最新版本中都有加入一个叫pixiedust的新的破解方式，
对于有限的几个路由器可以秒破。具体原理见下方的链接。

### 王卡，3070辟谣

前几年有专门销售号称可以蹭网的蹭网卡，王卡，使用3070芯片的卡，其实就是一个比较便宜的（RT3070芯片比较便宜的）但是信号不错的而且支持moitor/injection模式的网卡。
并不是非要用这种卡。

## Linux 下的无线简述

Linux内核无线子模块有mac80211，cfg80211，nl80211。mac80211是无线设备驱动的API，也就是编写无线设备驱动的框架。
使用mac80211 API编写的驱动是SoftMac驱动，其在驱动层面上进行链路层上的访问控制。与之对应的是FullMac驱动，
在硬件上进行访问控制。cfg80211是无线设备配置在内核态中的部分。nl80211是无线设备配置在用户空间的部分。

在早些时候，Linux使用的无线驱动和配置模块叫做WE/WEXT/Wireless Extension，被cfg80211替代。

使用cfg80211和nl80211的用户空间程序有iw，rfkill，hostapd，wpa_supplicant等，
详见[此链接](https://wireless.wiki.kernel.org/en/users/documentation)。

详细信息参考[Linux Wireless](https://wireless.wiki.kernel.org/)，该网站上还有Linux上面的无线驱动/固件/设备的相关资料。

## 链接

### 网卡的选购和常识

* [无线安全需要了解的芯片选型、扫描器使用知识](http://www.freebuf.com/articles/wireless/33524.html)
* [推荐无线设备](http://www.blackmoreops.com/2014/01/08/recommended-usb-wireless-cards-kali-linux/)

### 如何破解WiFi

* [从WIFI破解到无线安全内网渗透](http://www.qshare.cn/3896.html)
* [Ubuntu下Aircrack-ng工具包的使用](http://netsecurity.51cto.com/art/201111/304171.htm)

### 杂项

* [用来查询常见设备使用的芯片的开放WiKi](https://wikidevi.com/wiki/Main_Page)
* [Pixie-Dust WPS快速破解漏洞](https://forums.kali.org/showthread.php?24286-WPS-Pixie-Dust-Attack-&#40;Offline-WPS-Attack&#41;)
* [Pixie-Dust提出时的PPT](Hacklu2014_offline_bruteforce_attack_on_wps.pdf)
* [802.11规范](802.11-2012.pdf)
* [WPS 规范](Wi-Fi_Simple_Configuration_Technical_Specification_v2.0.5.pdf)
* [Linux Wireless](https://wireless.wiki.kernel.org/)