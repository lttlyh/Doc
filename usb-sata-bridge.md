# USB/SATA桥 芯片资料

买USB转SATA的转接线或适配器一个主要关注点就是他所使用的芯片的型号。

## USB接口的带宽

usb2.0最大带宽480Mbps，半双工，USB3.0最大带宽5Gbps，全双工。USB3.1最大带宽10Gbps。

## UAS

UAS(Universal Attached SCSI protocol)支持该协议的芯片能够提供更快的速度。

## 各厂商芯片

### ASMedia

* asm1153 - usb3.0/sata2.6(3Gbps)/uas
* asm1153e - usb3.0/sata3(6Gbps )/uas
* asm1351 - usb3.1/sata3.2/uas
* asm1352R - usb3.1/sata3.2 x 2/uas

### Norelsys

* ns1066/ns1066x - usb3.0/sata2.6 sata3(带x的型号)/无uas(规格书中没有)
* ns1068/ns1068x - usb3.0/sata2.6 sata3(带x的型号)/uas(需要自定义固件，应该都被支持的吧)

### Jmicron

* jms567 usb3.0/sata3.0/uas(需要jmf667)
* jms568 usb3.0/sata3.0/uas
* jm20329/20337 usb2.0/sata2.5
* jms355 usb2.0 eSATA/sata2.6
* jms561 usb3.0/sata3.0/uas/raid
* jms561u usb3.0/sata3.0/uas/clone raid
* jms562 usb3.0/sata3.0/uas/raid

### NEC/Renesas

基本没有用的，懒得搜集了。

* µPD720231A

[一个BBS上关于桥接芯片的讨论](http://bbs.mydigit.cn/simple/?t1086044.html)
[Norelsys](http://www.norelsys.com/Chinese)
[ASMedia](http://www.asmedia.com.tw)
[Jmicron](jmicron.com/)
[Renesas](http://am.renesas.com/)