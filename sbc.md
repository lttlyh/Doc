# SBC (Single Board Computer) 资料

`SBC`是单板电脑的意思，通常就是开发板的意思。常见的开发板包括`Raspberry Pi`,`Banana Pi`,`什么什么 Pi`,
`BegaleBoard`,`CubieTruck`,`Jetson TK/TX`。在[eLinux](http://www.elinux.org/Main_Page)
上面有常见的许多的开发板的列表和资料。

这里是一些与SBC相关的资料。

## 串口连接RPI(Raspberry Pi)

![300px-Adafruit-connection.jpg](300px-Adafruit-connection.jpg)

通过串口链接树莓派，可以直接在电脑上打开一个到树莓派的命令行终端，而不需要在树莓派上连接显示器，键盘，鼠标。
算是一种比较简单的远程登陆的方式。*串口连接树莓派的体验就和计算机网络课上面串口连接路由器的差不多*

串口连接树莓派用的是TTL电平规范的接口，X宝上面有很多USB到TTL的适配器，常用的适配芯片有`CP2102`,
`PL2302`,`FT232`,`CH340`等等，X宝上面搜索对应型号即可看到相关的产品，便宜的不到10块钱。
传说Win 10上面驱动可以自动安装，但是Win 7上面需要手动安装。

接下来只要参考[RPi Serial Connection](http://elinux.org/RPi_Serial_Connection)即可。

### UART/TTL/RS232/RS485/RS422

UART是通讯协议，TTL/RS232/RS485/RS422是电平规范，两者配合才能够进行实际的通信。UART将数据调制成0/1表示的
比特流，在通过具体的电平规范发送出去。TTL是0/3.3V，RS232是-12V/12V。RS485/RS422不清楚。

## 显示设备

树莓派上面能够驱动显示屏的接口总共有三个，SPI，MIPI/DSI，HDMI。MIPI/DSI基本上不需要考虑，
这种接口的显示屏通常需要一些固件才能够驱动，而这些固件通常不好找。SPI接口的数据量太低，
显示效果如同幻灯片，但是网上这方面的DIY资料还是挺多的。所以还是可以研究研究的。最可靠的是
HDMI显示方式，HDMI不需要固件，数据传输量比较大，而且X宝上面LVDS，MIPI/DSI到HDMI的转接板。

显示屏常见的显示屏信号线规范有LVDS，MIPI/DSI，eDP等。

在开发板上使用显示屏的可能方案（我没实际操作过）。先搞一块屏幕，可以去panelook看，
也可以搜一搜各种手机平板的屏幕总成。一般屏幕是LVDS，或者MIPI/DSI接口的。
但是屏幕引出的线是软排线，不好操作。可以在X宝上面买一个对应pin数和间距的软排线到杜邦接口的转接板
然后就是用杜邦线把屏幕和LVDS或MIPI到HDMI的转接板连接起来。最后处理下背光和触屏的接线。

## ARM CPU架构

![V5_to_V8_Architecture.jpg](V5_to_V8_Architecture.jpg)

## MISC

* [eLinux:各种开发板的Wiki](http://www.elinux.org/Main_Page)
* [MinnowBoard:Intel的开源开发板 1000块左右](http://wiki.minnowboard.org/MinnowBoard_Wiki_Home)
* [RPi Hub：Raspbery Pi的Wiki](http://elinux.org/RPi_Hub)
* [Raspbian：Raspberry Pi的Debian操作系统主页](http://www.raspbian.org/)
* [MIPI/DSI 屏幕数据线规范](http://mipi.org/specifications/display-interface)
* [各种大小的显示屏型号资料大全](http://www.panelook.com/)